<?php
	session_start();
	include 'fonctions/pour_tous.php';
	include 'fonctions/pour_prof.php';
	if (isset($_SESSION['matr_prof'])){
?>
<!DOCTYPE html>
<html>
<head>
	<title>Interface Professeur</title>
	<link rel="stylesheet" type="text/css" href="style/style.css"/>
</head>
<body>
	<?php 

		echo '<h4>'.infos_prof($_SESSION['matr_prof'])[0]['prenom'].' '.infos_prof($_SESSION['matr_prof'])[0]['nom'].'</h4>';
	?>
	<form action="model/model_ajout_examen.php" method="post">		
		<fieldset>
			<legend>Examen</legend>

			<label>Matiere de l'examen</label>
			<select name="nom-matiere">
				<option hidden selected></option>
					<?php
						$donnes = recup_matieres_enseignees($_SESSION['matr_prof']);
						foreach ($donnes as $cle) {
							echo '<option>'.$cle['nom'] .'</option>';
						}
					?>
			</select>
			<label>Classe(s) concernees</label><br>
				<?php
					$donnees = recup_classes_enseignees($_SESSION['matr_prof']);
					foreach ($donnees as $key) {
						echo '<input type="checkbox" name="classe[]" value="'.$key['nom'].'">'. $key['nom'] .'</input>';
						echo "<br>";
					}
				?>
			<br>
			<input type="submit" value="Preparer"></input>
		</fieldset>	
	</form>

			<?php
				foreach ($donnes as $d) {					
					$examens = recup_examen_matiere($d['id']);
					foreach ($examens as $e) {
						echo '<form method="post" action="model/model_liste_etud.php">';
						echo "<fieldset>";
						echo '<legend>Corriger N* '.$e['code'].'</legend>';
						echo '<input type="submit" value="'.$d['nom'].'"/>';
						echo '<input type="text" name="code-examen" hidden value="'.$e['code'].'"/>';
						echo '<input type="text" name="id-matiere" hidden value="'.$key['id'].'"/>';
						echo "</fieldset>";
						echo '</form>';
					}
				}
			?>

		
</body>
</html>
<?php
	}else{
		header('Location: prof.php');
	}
?>