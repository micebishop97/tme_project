<?php	
	include 'fonctions/pour_admin.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Interface Administrateur</title>
	<link rel="stylesheet" type="text/css" href="style/style.css"/>
	<style type="text/css">
			form{
				display: none;
			}
	</style>
	<script type="text/javascript" src="script/script.js"></script>
</head>
<body>
	<h1>Bienvenue dans l'espace Administrateur</h1>
	<!-- Gestion des CLasses -->
	<div>
		<h4>Classes:</h4>
		<input type="button" name='ajout_classe' value="Ajouter une classe" onclick="displayHide('form-ajout-classe')">

		<form method="post" action="model/model_ajout_classe.php" id="form-ajout-classe">
			<fieldset>
				<legend>Ajouter Classe</legend>
				<input type="text" placeholder="Nom Classe" name="nom-classe" required></input> <br>
				<input type="submit" value="Ajouter"></input>
				<input type="reset" value="Tout Effacer"></input>
			</fieldset>
		</form>

		<input type="button" name='supp_classe' value="Supprimer une classe" onclick="displayHide('form-supp-classe')">
		<form method="post" action="model/model_supp_classe.php" id="form-supp-classe">
			<fieldset>
				<legend>Supprimer Classe</legend>
				<select name="liste-classes" id="liste-classes" required>
					<option hidden selected></option>
					<?php
						$donnees = liste_classes();
						foreach ($donnees as $key => $value) {	
							$result	= $value['nom'];
							echo '<option name>'. $result .'</option>';
						}
					?>
				</select>
				<input type="submit" value="Supprimer">
			</fieldset>
		</form>
	</div>
	<!-- Fin Gestion des classes -->

	<!-- Gestion des Etudiants -->
	<div>
		<h4>Etudiants:</h4>
		<input type="button" name='ajout_etudiant' value="Ajouter un etudiant" onclick="displayHide('form-ajout-etud')">
		<form method="post" action="model/model_ajout_etud.php" id="form-ajout-etud">
			<fieldset>
				<legend>Ajouter Etudiant</legend>
				<input type="text" name="nom-etudiant" placeholder='Nom' required> <br>
				<input type="text" name="pnom-etudiant" placeholder='Prenom' required> <br>
				<input type="date" name="ddn-etudiant" required> <br>
				<input type="text" name="adr-etudiant" placeholder='Adresse'> <br>
				<input type="text" name="numtel-etudiant" placeholder='Numero de telephone' required> <br>
				<select name="liste-classes" id="liste-classes" required=>
					<option hidden selected></option>
					<?php
						$donnees = liste_classes();
						foreach ($donnees as $key => $value) {	
							$result	= $value['nom'];
							echo '<option name>'. $result .'</option>';
						}
					?>
				</select>
				<input type="submit" value="Ajouter">
			</fieldset>
		</form>
		<input type="button" name='supp_etudiant' value="Supprimer un etudiant" onclick="displayHide('form-supp-etud')">
		<form method="post" action="model/model_supp_etud.php" id="form-supp-etud">
			<fieldset>
				<legend>Supprimer Etudiant</legend>
				<select name="liste-etudiants" id="liste-etudiants" required>
					<option hidden selected></option>
					<?php
						$donnees = liste_etudiants();
						foreach ($donnees as $key => $value) {	
							$result_code	= $value['code'];
							$result_pnom = $value['prenom'];
							$result_nom	= $value['nom'];
							$result_ddn	= $value['ddn'];							
							echo '<option name>'. $result_code .' '. $result_pnom .' '. $result_nom .' '. $result_ddn.'</option>';
						}
					?>
				</select>
				<input type="submit" value="Supprimer">
			</fieldset>
		</form>
	</div>
	<!-- Fin Gestion Etudiants -->
	<!-- Gestion des professeurs -->
	<div>
		<h4>Professeurs:</h4>
		<input type="button" name='ajout_prof' value="Ajouter un professeur" onclick="displayHide('form-ajout-prof')">
		<form method="post" action="model/model_ajout_prof.php" id="form-ajout-prof">
			<fieldset>
				<legend>Ajouter Professeur</legend>
				<input type="text" name="nom-prof" placeholder='Nom' required> <br>
				<input type="text" name="pnom-prof" placeholder='Prenom' required> <br>
				<input type="text" name="adr-prof" placeholder='Adresse'> <br>
				<input type="text" name="numtel-prof" placeholder='Numero de telephone' required> <br>
				<input type="submit" value="Ajouter">
			</fieldset>
		</form>
		<input type="button" name='supp_prof' value="Supprimer un professeur" onclick="displayHide('form-supp-prof')">
		<form method="post" action="model/model_supp_prof.php" id="form-supp-prof">
			<fieldset>
				<legend>Supprimer Professeur</legend>
				<select name="liste-profs" id="liste-profs" required>
					<option hidden selected></option>
					<?php
						$donnees = liste_profs();
						foreach ($donnees as $key => $value) {	
							$result_matr = $value['matr'];
							$result_pnom = $value['prenom'];
							$result_nom	= $value['nom'];							
							echo '<option name>'. $result_matr .' '. $result_pnom .' '. $result_nom .'</option>';
						}
					?>
				</select>
				<input type="submit" value="Supprimer">
			</fieldset>
		</form>
	</div>
	<!-- Fin gestion des professeurs -->
	<!-- Gestion des matieres -->
	<div>
		<h4>Matieres:</h4>
		<input type="button" name='ajout_mat' value="Ajouter une matiere" onclick="displayHide('form-ajout-mat')">
		<form method="post" action="model/model_ajout_matiere.php" id="form-ajout-mat">
			<fieldset>
				<legend>Ajouter Matiere</legend>
				<input type="text" name="nom-mat" placeholder='Nom' required> <br>
				<input type="submit" value="Ajouter">
			</fieldset>
		</form>
		<input type="button" name='supp_mat' value="Supprimer une matiere" onclick="displayHide('form-supp-mat')">
		<form method="post" action="model/model_supp_matiere.php" id="form-supp-mat">
			<fieldset>
				<legend>Supprimer Matiere</legend>
				<select name="liste-mats" id="liste-mats" required>
					<option hidden selected></option>
					<?php
						$donnees = liste_matieres();
						foreach ($donnees as $key => $value) {	
							$result_nom	= $value['nom'];							
							echo '<option name>'. $result_nom .'</option>';
						}
					?>
				</select>
				<input type="submit" value="Supprimer">
			</fieldset>
		</form>
	</div>
	<!-- Fin gestion des matieres -->
	<!-- Gestion des Assignations -->
	<div>
		<h4>Assgnation Prof-Classe-MatiereE:</h4>
		<input type="button" name='ajout_ass' value="Ajouter une assignation" onclick="displayHide('form-ajout-ass')">
		<form method="post" action="model/model_ajout_ass.php" id="form-ajout-ass">
			<fieldset>
				<legend>Ajouter Assignation</legend>
				<select name="liste-profs" id="liste-profs" required>
					<option hidden selected></option>
					<?php
						$donnees = liste_profs();
						foreach ($donnees as $key => $value) {	
							$result_matr = $value['matr'];
							$result_pnom = $value['prenom'];
							$result_nom	= $value['nom'];							
							echo '<option name>'. $result_matr .' '. $result_pnom .' '. $result_nom .'</option>';
						}
					?>
				</select>

				<span>enseigne</span>

				<select name="liste-mats" id="liste-mats" required>
					<option hidden selected></option>
					<?php
						$donnees = liste_matieres();
						foreach ($donnees as $key => $value) {	
							$result_nom	= $value['nom'];							
							echo '<option name>'. $result_nom .'</option>';
						}
					?>
				</select>

				<span>dans la classe de</span>

				<select name="liste-classes" id="liste-classes" required>
					<option hidden selected></option>
					<?php
						$donnees = liste_classes();
						foreach ($donnees as $key => $value) {	
							$result	= $value['nom'];
							echo '<option name>'. $result .'</option>';
						}
					?>
				</select>
				<input type="submit" value="Ajouter">
			</fieldset>
		</form>
		<input type="button" name='supp_ass' value="Supprimer une assignation" onclick="displayHide('form-supp-ass')">
		<form method="post" action="model/model_supp_ass.php" id="form-supp-ass">
			<fieldset>
				<legend>Supprimer Assignation</legend>
				<select name="liste-ass" id="liste-ass" required>
					<option hidden selected></option>
					<?php
						$donnees = liste_assignations();
						foreach ($donnees as $key => $value) {
							$result_matr = $value['matr_prof'];
							$result_id_matiere = $value['id_matiere'];
							$result_id_classe = $value['id_classe'];
							$result_pnom = $value['prenom_prof'];
							$result_nom	= $value['nom_prof'];								
							$result_nom_mat = $value['nom_matiere'];								
							$result_nom_classe = $value['nom_classe'];
							
							echo '<option>'.$result_matr.' '. $result_pnom .' '. $result_nom .' '. $result_nom_mat .' '.$result_nom_classe.'</option>';							
						}

					?>
				</select>
				<input type="submit" value="Supprimer">
			</fieldset>
		</form>
	</div>
	<!-- Fin gestion des assignations -->
	<form method="post" action="deconnexion.php">
		<input type="submit" value="Deconnxeion"></input>
	</form>
	<br>
	<br>
	<br>
	<a href="deconnexion.php"><input type="button" value="Deconnexion"></input></a>
</body>
</html>