<?php
	if (isset($_POST['code-examen'])) {
		$code_examen = $_POST['code-examen'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Examen</title>
	<script type="text/javascript" src='script/script.js'></script>
	<style type="text/css">
	.examen{
		display: none;
	}
	</style>
</head>
<body>
	<input type="button" value="Commencer l'examen" onclick="document.getElementById('examen').style.display='block';"/>
	<form action="model/model_ajout_reponses.php" method="post">
		<?php
			include 'fonctions/pour_tous.php';
			include 'fonctions/pour_etud.php';
			$questions = recup_questions($code_examen);	
			echo "<div class='examen' id='examen'>";
			foreach ($questions as $q) {
				if ($q['type']=='qr'){					
					echo '<label>'.$q['libelle'].'</label><br>';
					echo "<textarea name='reponse-qr[]' cols='100' rows'13'></textarea>";
					echo "<br>";
					echo "<br>";
				}
				else{
					echo '<label>'.$q['libelle'].'</label><br>';
					$reponses = recup_reponses_prof($q['id']);
					echo '<ul>';	
					foreach ($reponses as $r) {
						echo '<li><input type="checkbox" name="reponse-qcm[]" value="'.$r['contenu'].'" >'.$r['contenu'].'</input></li>';
					}
					echo '</ul>';					
					echo "<br>";
				}
			}
			echo '<input type="submit" value="Soumettre"></input>';
			echo "</div>";
		?>

	</form>
</body>
</html>
<?php
	}else{
		echo "Pas de code d'examen envoye";
	}
?>