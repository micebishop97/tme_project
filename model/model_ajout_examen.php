<?php

?>

<!DOCTYPE html>
<html>
<head>
	<title>Accuse de Reception</title>
	<style type="text/css">
		fieldset.fs-lv2{
			display: none;
		}
	</style>
	<script type="text/javascript" src="../script/script.js"></script>
</head>
<body>
	<?php
		include('../fonctions/pour_admin.php');
		include('../fonctions/pour_prof.php');
		if (isset($_POST['nom-matiere']) && isset($_POST['classe'])){
			$id_matiere = recup_id_matiere($_POST['nom-matiere']);
			if ($code_examen=ajouter_examen($id_matiere)){
				echo "Examen Ajoute<br>";
				foreach ($_POST['classe'] as $nom_classe) {
					$liste_etud = liste_etudiants_classe(recup_id_classe($nom_classe));
					foreach ($liste_etud as $l) {
						ajouter_examen_a_faire($l['code'], $code_examen);
						echo 'Assignee a l eleve '.$l['prenom'].' '.$l['nom'].'<br>';
					}
					echo 'Assignee a la classe '.$nom_classe.'<br>';
				}
	?>
	<form method="post" action="model_ajout_qr.php">
			<input type="button" value="Ajouter Q/R" onclick="displayHide('fs-lv2-qr')"></input>
			<fieldset class="fs-lv2" id="fs-lv2-qr">
				<legend>Q/R</legend>
				<textarea name="question" placeholder="Question ?" rows=5 cols="100" required></textarea>
				<br>
				<textarea name="reponse" placeholder="Reponse attendue." rows="5" cols="100" required></textarea>
				<br>
				<label>Note sur:</label><input name="nb-points" type="number" required></input>Duree:<input name="duree" type="number"></input><br>
				<br>
				<P>
					<input type="submit" value="Ajouter"></input>
					<input type="reset" value="Tout Effacer"></input>
				</P>
				<input type="text" value='<?php echo $code_examen; ?>' name="code-examen"  hidden></input>
			</fieldset>			
		</form>

		<form method="post" action="model_ajout_qcm.php">
			<input type="button" value="Ajouter QCM" onclick="displayHide('fs-lv2-qcm')"></input>
			<fieldset class="fs-lv2" id="fs-lv2-qcm" >
				<legend>QCM</legend>
				<textarea name="question" placeholder="Question ?" rows=5 cols="100" required></textarea>
				<br>
				<input name="reponse1" type="text" placeholder="Choix1"></input>points:<input type="number" name="nb-points1"/><br>
				<input name="reponse2" type="text" placeholder="Choix2"></input>points:<input type="number" name="nb-points2"/><br>
				<input name="reponse3" type="text" placeholder="Choix3"></input>points:<input type="number" name="nb-points3"/><br>
				<input name="reponse4" type="text" placeholder="Choix4"></input>points:<input type="number" name="nb-points4"/><br>
				</input>Duree:<input name="duree" type="number"></input>
			<P>
				<input type="submit" value="Ajouter"></input>
				<input type="reset" value="Tout Effacer"></input>
			</P>
			<input type="text" value='<?php echo $code_examen; ?>' name="code-examen"  hidden></input>
			</fieldset>
		</form>
	<?php

			}else{
				echo "Erreur lors de l'ajout";
			}
		}else{
			echo "Les informations n'ont pas ete envoyees";
		}
	?>
	<input type="button" onclick="document.location.href='../interface_prof.php'" value="Precedent"></input>
</body>
</html>