<!DOCTYPE html>
<html>
<head>
	<title>Modifier mot de passe</title>
</head>
<body>
	<form method="post">
		<input type="password" name="ancien-mdp" placeholder="Entrer ancien mot de passe" required></input></br>
		<input type="password" name="nouv-mdp" placeholder="Entrer nouveau mot de passe" required></input></br>
		<input type="password" name="nouv-mdp-enc" placeholder="Entrer nouveau mot de passe encore" required></input></br>
		<input type="submit" value="Changer"></input>
		<input type="reset" value="Tout effacer"></input>
		<input type="button" value="Precedant" onclick="document.location.href='../interface_etud.php'"></input>
	</form>

</body>
</html>
<?php
	session_start();
	include '../fonctions/pour_tous.php';
	include '../fonctions/pour_etud.php';
	if (isset($_POST['ancien-mdp']) && isset($_POST['nouv-mdp']) && isset($_POST['nouv-mdp-enc'])){
		$ancien_mdp = $_POST['ancien-mdp']; 
		$nouv_mdp = $_POST['nouv-mdp'];
		$nouv_mdp_enc = $_POST['nouv-mdp-enc'];
		if ($ancien_mdp != recup_mdp_etud($_SESSION['code_etudiant'])){
			echo "<script>alert('Vous connaissez pas votre mot de passe actuel !')</script>";
		}
		elseif ($nouv_mdp!=$nouv_mdp_enc){
			echo "<script>alert('Les mots de passe ne sont pas identiques !')</script>";
		}
		else{
			modifier_mdp_etud($_SESSION['code_etudiant'], $nouv_mdp);
			echo "<script>alert('Mot de passe modifie avec succes !')</script>";			
		}
	}
?>