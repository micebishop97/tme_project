<?php
	session_start();

	include '../fonctions/pour_admin.php';
	include '../fonctions/pour_etud.php';
	include '../fonctions/pour_prof.php';
	if (isset($_POST['login']) && isset($_POST['mdp']) && $_POST['login']!='' && $_POST['mdp']!=''){
		$login = $_POST['login'];
		$mdp =$_POST['mdp'];
		if (admin_existe($login,$mdp)){
			header('Location: ../interface_admin.php');
		}else{
			echo "Vous n'etes pas administrateur";
		}
	}elseif (isset($_POST['code']) && isset($_POST['mdp']) && $_POST['code']!='' && $_POST['mdp']!='') {
		$code = $_POST['code'];
		$mdp =$_POST['mdp'];

		if (etudiant_existe($code,$mdp)){
			$_SESSION['code_etudiant']=$_POST['code'];
			header('Location: ../interface_etud.php');
		}else{
			echo "Vous ne faites pas partie de notre etablissement !";
		}
	}elseif (isset($_POST['matr']) && isset($_POST['mdp']) && $_POST['matr']!='' && $_POST['mdp']!='') {
		$matr = $_POST['matr'];
		$mdp =$_POST['mdp'];

		if (prof_existe($matr,$mdp)){
			$_SESSION['matr_prof']=$_POST['matr'];
			header('Location: ../interface_prof.php');
		}else{
			echo "Vous n'etes pas prof dans notre etablissement !";
		}
	}else{
		header('Location: ../prof.php');
		echo "/!\ Entrez votre login et votre mot de passe pour vous connecter !";
	}
?>