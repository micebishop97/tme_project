function displayHide(id){
	var input = document.getElementById(id);
	if (input.style.display == 'block')
		input.style.display = 'none';
	else
		input.style.display = 'block';

}

function suivant(num){
	if (num!=4) {
		document.getElementById('q'+(num)).style.display='none';
		document.getElementById('q'+(num+1)).style.display='block';
	}
}

function precedant(num){
	if(num!=1){
		document.getElementById('q'+(num)).style.display='none';
		document.getElementById('q'+(num-1)).style.display='block';
	}
}


function timer(num){
	var div = document.getElementById('q'+num+1);
	var duree = document.getElementById('duree'+num);
	var val_duree = parseInt(duree.innerHTML);
	setInterval(function(){		
		val_duree--;
		duree.innerHTML=val_duree;
	}, 1000)
	if(document.getElementById('p').clicked){
		suivant(num);
	}
	else{
		if (document.getElementById('s').clicked) {
			precedant(num);
		}
	}
}