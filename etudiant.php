<!DOCTYPE html>
<html>
<head>
	<title>Etudiant</title>
	<link rel="stylesheet" type="text/css" href="style/style.css"/>
</head>
<body>
	<form method="post" action="model/model_connexion.php">
		<fieldset>
			<legend>Connexion</legend>
			<p>
				<input type="text" placeholder="Code" id="code" name="code"></input>
			</p>
			<p>
				<input type="password" placeholder="Mot de passe" id="mdp" name="mdp"></input>
			</p>
			<input type="submit" value="Se Connecter"></input>
		</fieldset>
	</form>	
	<input type="button" onclick="document.location.href='index.html'" value="Accueil"></input>
</body>
</html>