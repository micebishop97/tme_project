<!DOCTYPE html>
<html>
<head>
	<title>Administrateur</title>
	<link rel="stylesheet" type="text/css" href="style/style.css"/>
</head>
<body>
	<form method="post" action="model/model_connexion.php">
		<fieldset>
			<legend>Connexion</legend>
			<p>
				<input type="text" placeholder="Login" id="login" name="login"></input>
			</p>
			<p>
				<input type="password" placeholder="Mot de passe" id="mdp" name="mdp"></input>
			</p>
			<input type="submit" value="Se Connecter"></input>
		</fieldset>
	</form>	
	<input type="button" onclick="document.location.href='index.html'" value="Accueil"></input>
</body>
</html>