<?php
	session_start();
	include 'fonctions/pour_tous.php';
	include 'fonctions/pour_etud.php';
	// include 'fonctions/pour_admin.php';
	if(isset($_SESSION['code_etudiant'])){
?>
<!DOCTYPE html>
<html>
<head>
	<title>Interface Etudiant</title>
	<link rel="stylesheet" type="text/css" href="style/style.css"/>
</head>
<body>
	<h1>Bienvenue dans l'interface de l'etudiant</h1>
	<input type="button" value="Changer mot de passe" onclick="document.location.href='model/model_mod_mdp_etud.php'"></input>
	<?php 
		$donnees_etud = recup_donnees_etud($_SESSION['code_etudiant']);
		// echo $_SESSION['code_etudiant'];
		// print_r($donnees);
		// print_r(recup_donnees_etud($_SESSION['code_etudiant']));
		$donnees_classe = recup_donnees_classe($donnees_etud[0]['id_classe']);

		echo '<h4>'.$donnees_etud[0]['prenom']. ' '. $donnees_etud[0]['nom'] . ' '. $donnees_classe[0]['nom']. '</h4>';
	?>
	
		<ul>
			<?php
				$exam = examens_a_faire($_SESSION['code_etudiant']);

				echo '<h4>Examen a faire:</h4>';
				foreach ($exam as $key) {
					$donnees_exam = infos_exam($key['code_examen'], $donnees_etud[0]['id_classe']);

					foreach ($donnees_exam as $cle) {
						echo '<form method="post" action="faire_examen.php">';
						echo '<input type=\'submit\' value=\''.'Examen de '.nom_matiere_exam($cle['code'])[0]['nom'].'\'>';
						?>
						<input type="text" name="code-examen" hidden value=<?php echo $cle['code']?> ></input>
						<?php
						echo "</form>";
					}
				}
			?>
		</ul>
	<form action="deconnexion.php" method="post">
		<input type="submit" id="deco" value="Deconnexion"></input>
	</form>
</body>
</html>
<?php
}else{
	header('Location: etudiant.php');
}
?>
