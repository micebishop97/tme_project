-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: tme_project
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `mdp` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'micebishop','passer');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classe`
--

DROP TABLE IF EXISTS `classe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classe`
--

LOCK TABLES `classe` WRITE;
/*!40000 ALTER TABLE `classe` DISABLE KEYS */;
INSERT INTO `classe` VALUES (14,'DESCAF2'),(13,'DST2A'),(5,'DUT2');
/*!40000 ALTER TABLE `classe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enseigner`
--

DROP TABLE IF EXISTS `enseigner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enseigner` (
  `matr_prof` varchar(30) DEFAULT NULL,
  `id_matiere` int(11) DEFAULT NULL,
  `id_classe` int(11) DEFAULT NULL,
  KEY `matr_prof` (`matr_prof`),
  KEY `id_matiere` (`id_matiere`),
  KEY `id_classe` (`id_classe`),
  CONSTRAINT `enseigner_ibfk_1` FOREIGN KEY (`matr_prof`) REFERENCES `prof` (`matr`) ON DELETE SET NULL,
  CONSTRAINT `enseigner_ibfk_2` FOREIGN KEY (`id_matiere`) REFERENCES `matiere` (`id`) ON DELETE SET NULL,
  CONSTRAINT `enseigner_ibfk_3` FOREIGN KEY (`id_classe`) REFERENCES `classe` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enseigner`
--

LOCK TABLES `enseigner` WRITE;
/*!40000 ALTER TABLE `enseigner` DISABLE KEYS */;
INSERT INTO `enseigner` VALUES ('pAS12J38R',1,5),('pSDN23V03',2,5);
/*!40000 ALTER TABLE `enseigner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etudiant`
--

DROP TABLE IF EXISTS `etudiant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etudiant` (
  `code` varchar(30) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `adr` varchar(50) DEFAULT NULL,
  `ddn` date NOT NULL,
  `mdp` varchar(30) NOT NULL,
  `numtel` varchar(9) NOT NULL,
  `id_classe` int(11) DEFAULT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `numtel` (`numtel`),
  KEY `id_classe` (`id_classe`),
  CONSTRAINT `etudiant_ibfk_1` FOREIGN KEY (`id_classe`) REFERENCES `classe` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etudiant`
--

LOCK TABLES `etudiant` WRITE;
/*!40000 ALTER TABLE `etudiant` DISABLE KEYS */;
INSERT INTO `etudiant` VALUES ('eV8OI4LUR','NDIAYE','Papa Seynou Sy','Maristes','1997-03-30','mdpCTSI2DET','776097715',5),('eYB3MBU3K','MBENGUE','Arame','Scat Urbam','1996-09-07','mdp2OEMPK9N','777449386',13);
/*!40000 ALTER TABLE `etudiant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examen`
--

DROP TABLE IF EXISTS `examen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examen` (
  `code` varchar(30) NOT NULL,
  `id_matiere` int(11) NOT NULL,
  PRIMARY KEY (`code`),
  KEY `id_matiere` (`id_matiere`),
  CONSTRAINT `examen_ibfk_1` FOREIGN KEY (`id_matiere`) REFERENCES `matiere` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examen`
--

LOCK TABLES `examen` WRITE;
/*!40000 ALTER TABLE `examen` DISABLE KEYS */;
INSERT INTO `examen` VALUES ('xFSPJ12IE',1),('xKJ23K33P',2);
/*!40000 ALTER TABLE `examen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examen_a_faire`
--

DROP TABLE IF EXISTS `examen_a_faire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examen_a_faire` (
  `code_etudiant` varchar(30) NOT NULL,
  `code_examen` varchar(30) NOT NULL,
  KEY `code_etudiant` (`code_etudiant`),
  KEY `code_examen` (`code_examen`),
  CONSTRAINT `examen_a_faire_ibfk_1` FOREIGN KEY (`code_etudiant`) REFERENCES `etudiant` (`code`) ON DELETE CASCADE,
  CONSTRAINT `examen_a_faire_ibfk_2` FOREIGN KEY (`code_examen`) REFERENCES `examen` (`code`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examen_a_faire`
--

LOCK TABLES `examen_a_faire` WRITE;
/*!40000 ALTER TABLE `examen_a_faire` DISABLE KEYS */;
INSERT INTO `examen_a_faire` VALUES ('eV8OI4LUR','xFSPJ12IE'),('eV8OI4LUR','xKJ23K33P');
/*!40000 ALTER TABLE `examen_a_faire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matiere`
--

DROP TABLE IF EXISTS `matiere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matiere` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matiere`
--

LOCK TABLES `matiere` WRITE;
/*!40000 ALTER TABLE `matiere` DISABLE KEYS */;
INSERT INTO `matiere` VALUES (1,'algo'),(2,'maths');
/*!40000 ALTER TABLE `matiere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prof`
--

DROP TABLE IF EXISTS `prof`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prof` (
  `matr` varchar(30) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `adr` varchar(50) DEFAULT NULL,
  `mdp` varchar(30) NOT NULL,
  `numtel` varchar(9) NOT NULL,
  PRIMARY KEY (`matr`),
  UNIQUE KEY `numtel` (`numtel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prof`
--

LOCK TABLES `prof` WRITE;
/*!40000 ALTER TABLE `prof` DISABLE KEYS */;
INSERT INTO `prof` VALUES ('pAS12J38R','KA','Adama','Fouta','passer','771234567'),('pSDN23V03','OUYA','Samuel','Cote d\'Ivoire','passer','777654321');
/*!40000 ALTER TABLE `prof` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` text NOT NULL,
  `duree` int(11) NOT NULL,
  `code_examen` varchar(30) NOT NULL,
  `type` enum('qcm','qr') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code_examen` (`code_examen`),
  CONSTRAINT `question_ibfk_1` FOREIGN KEY (`code_examen`) REFERENCES `examen` (`code`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'Qui est l\'actuel president de la Republique',1,'xFSPJ12IE','qcm'),(2,'Quel langage precede le langage C',2,'xFSPJ12IE','qcm'),(3,'Expliquez clairement ce qu\'est un serveur web',4,'xFSPJ12IE','qr'),(4,'A quoi consiste le pagination',3,'xFSPJ12IE','qr');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reponse_etudiant`
--

DROP TABLE IF EXISTS `reponse_etudiant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reponse_etudiant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_question` int(11) NOT NULL,
  `contenu` text NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0',
  `code_etudiant` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_question` (`id_question`),
  KEY `code_etudiant` (`code_etudiant`),
  CONSTRAINT `reponse_etudiant_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `question` (`id`) ON DELETE CASCADE,
  CONSTRAINT `reponse_etudiant_ibfk_2` FOREIGN KEY (`code_etudiant`) REFERENCES `etudiant` (`code`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reponse_etudiant`
--

LOCK TABLES `reponse_etudiant` WRITE;
/*!40000 ALTER TABLE `reponse_etudiant` DISABLE KEYS */;
/*!40000 ALTER TABLE `reponse_etudiant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reponse_prof`
--

DROP TABLE IF EXISTS `reponse_prof`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reponse_prof` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_question` int(11) NOT NULL,
  `contenu` text NOT NULL,
  `points` int(11) NOT NULL,
  `commentaire` text,
  PRIMARY KEY (`id`),
  KEY `id_question` (`id_question`),
  CONSTRAINT `reponse_prof_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `question` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reponse_prof`
--

LOCK TABLES `reponse_prof` WRITE;
/*!40000 ALTER TABLE `reponse_prof` DISABLE KEYS */;
INSERT INTO `reponse_prof` VALUES (1,1,'Abdoulaye Wade',-1,NULL),(2,1,'Macky Sall',2,NULL),(3,1,'L. S. Senghor',-1,NULL),(4,1,'Abdou Diouf',-1,NULL),(5,2,'C++',-1,NULL),(6,2,'C--',-1,NULL),(7,2,'B',3,NULL),(8,2,'Python',-1,NULL),(9,3,'Un serveur web est un .... en tout cas c\'est ca.',5,NULL),(10,4,'La pagination est un concept utilise par le SE .... en tout cas c\'est ca.',10,'');
/*!40000 ALTER TABLE `reponse_prof` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resultat`
--

DROP TABLE IF EXISTS `resultat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resultat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_examen` varchar(30) NOT NULL,
  `code_etudiant` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code_examen` (`code_examen`),
  KEY `code_etudiant` (`code_etudiant`),
  CONSTRAINT `resultat_ibfk_1` FOREIGN KEY (`code_examen`) REFERENCES `examen` (`code`) ON DELETE CASCADE,
  CONSTRAINT `resultat_ibfk_2` FOREIGN KEY (`code_etudiant`) REFERENCES `etudiant` (`code`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resultat`
--

LOCK TABLES `resultat` WRITE;
/*!40000 ALTER TABLE `resultat` DISABLE KEYS */;
/*!40000 ALTER TABLE `resultat` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-04 18:27:16
