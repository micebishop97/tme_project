<?php
	function etudiant_existe($code, $mdp){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM etudiant WHERE code=:code AND mdp=:mdp');
			$req->execute(array(
					'code' => $code,
					'mdp' => $mdp
				));
		if ($req->fetch()) {
			return 1;
		}
		else{
			return 0;
		}
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}

	}

	function examens_a_faire($code_etudiant){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT code_examen FROM examen_a_faire WHERE code_etudiant=:code_etudiant');
			$req->execute(array(
					'code_etudiant' => $code_etudiant
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();

		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}

	}

	function recup_donnees_etud($code){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM etudiant WHERE code=:code');
			$req->execute(array(
					'code' => $code
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}

	}

	function recup_donnees_classe($id){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM classe WHERE id=:id');
			$req->execute(array(
					'id' => $id
				));

		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function infos_exam($code_examen, $id_classe){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT matr_prof, code, id_classe FROM enseigner INNER JOIN examen ON enseigner.id_matiere=examen.id_matiere WHERE code=:code_examen AND id_classe=:id_classe');
			$req->execute(array(
					'code_examen' => $code_examen,
					'id_classe' => $id_classe
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}
	}

	function recup_donnees_examen($code_examen){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM examen WHERE code=:code');
			$req->execute(array(
					'code' => $code
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}

	}

	function nom_matiere_exam($code_examen){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT nom FROM examen INNER JOIN matiere ON examen.id_matiere = matiere.id WHERE code=:code_examen');
			$req->execute(array(
					'code_examen' => $code_examen
				));

		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}

	function recup_questions($code_examen){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM question WHERE code_examen=:code_examen');
			$req->execute(array(
					'code_examen' => $code_examen
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}
	}

	function recup_reponses_prof($id_question){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM reponse_prof WHERE id_question=:id_question');
			$req->execute(array(
					'id_question' => $id_question
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}
	}

	function recup_mdp_etud($code_etudiant){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT mdp FROM etudiant WHERE code=:code_etudiant');
			$req->execute(array(
					'code_etudiant' => $code_etudiant
				));
		$resultSet = $req->fetch();
		return $resultSet['mdp'];
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}
	}

	function modifier_mdp_etud($code, $nouv_mdp){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('UPDATE etudiant SET mdp=:nouv_mdp WHERE code=:code');
			$req->execute(array(
					'nouv_mdp' => $nouv_mdp,
					'code' => $code
				));	
		return $nouv_mdp;

		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}
	}
	function ajouter_rep_etudiant($id_question,$contenu,$points,$code_etudiant){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO reponse_etudiant(id_question,contenu,points,code_etudiant) VALUES(:id_question,:contenu,:points,:code_etudiant)');
			$req->execute(array(
					'id_question' => $id_question,
					'contenu' => $contenu,
					'points' => $points,
					'code_etudiant' => $code_etudiant
				));
		
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}	
	} 
	function recup_resultat($code_examen,$code_etudiant){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM resultat WHERE code_examen=:code_examen AND code_etudiant=:code_etudiant');
			$req->execute(array(
					'code_examen' => $code_examen,
					'code_etudiant' => $code_etudiant
				));
		
		$resultSet=$req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}	

	}
?>