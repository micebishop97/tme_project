<?php
	// Connection a la base de donnees
	function connection_bdd(){
		try {
			$bdd = new PDO('mysql:host=127.0.0.1;dbname=tme_project', 'root', 'messi10'); // changer le mot de passe par le votre ou laissez le vide si vous en avez pas //
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (Exception $e) {
			die('Erreur de connection de la base de donnees: '.$e->getMessage());
		}
		return $bdd;
	}

	// Generation de code et de matricule aleatoire
	function generateRandomString($length = 8) {
	    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
    	return $randomString;
	}

?>