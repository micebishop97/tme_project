<?php
	function prof_existe($matr, $mdp){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM prof WHERE matr=:matr AND mdp=:mdp');
			$req->execute(array(
					'matr' => $matr,
					'mdp' => $mdp
				));
		if ($req->fetch()) {
			return 1;
		}
		else{
			return 0;
		}
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}

	}

	function recup_matieres_enseignees($matr_prof){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT DISTINCT matiere.id, matiere.nom FROM enseigner INNER JOIN matiere ON enseigner.id_matiere=matiere.id WHERE matr_prof=:matr_prof');
			$req->execute(array(
					'matr_prof' => $matr_prof
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}
	}
	function recup_donnees_prof($matr){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM prof WHERE matr=:matr');
			$req->execute(array(
					'matr'=> $matr
				));
		$resultSet=$req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}

	}
	function ajout_resultat($code_examen,$code_etudiant){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO resultat(code_examen,code_etudiant) VALUES(:code_examen,:code_etudiant)');
			$req->execute(array(
					'code_examen' => $code_examen,
					'code_etudiant' => $code_etudiant
				));
		
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}	

	}

	function recup_classes_enseignees($matr_prof){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT DISTINCT classe.id, classe.nom FROM enseigner INNER JOIN classe ON enseigner.id_classe=classe.id WHERE matr_prof=:matr_prof');
			$req->execute(array(
					'matr_prof' => $matr_prof
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}
	}

	function ajouter_examen($id_matiere){
		$bdd = connection_bdd();
		$code = 'x'.generateRandomString();
		try {
			$req = $bdd->prepare('INSERT INTO examen (code, id_matiere) VALUES(:code, :id_matiere)');
			$req->execute(array(
					'code' => $code,
					'id_matiere' => $id_matiere
				));
		return $code;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}

	function ajouter_examen_a_faire($code_etudiant, $code_examen){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO examen_a_faire (code_etudiant, code_examen) VALUES (:code_etudiant, :code_examen)');
			$req->execute(array(
					'code_etudiant' => $code_etudiant,
					'code_examen' => $code_examen
				));
		return $code_etudiant;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function ajouter_question($libelle, $duree, $type, $code_examen){
		echo "Bonjour";
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO question (libelle, duree, type, code_examen) VALUES(:libelle, :duree, :type, :code_examen)');
			$req->execute(array(
					'libelle' => $libelle,
					'duree' => $duree,
					'type' => $type,
					'code_examen' => $code_examen
				));
		var_dump($req);
		return $libelle;
		// $req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage());
			die(print_r($bdd->errorInfo()));
		}
		
	}

	function recup_id_question($libelle){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT id FROM question WHERE libelle=:libelle');
			$req->execute(array(
					'libelle' => $libelle,
				));
		$resultSet = $req->fetch();
		return $resultSet['id'];
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function ajouter_reponse_prof($contenu, $points, $id_question){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO reponse_prof (contenu, points, id_question) VALUES(:contenu, :points, :id_question)');
			$req->execute(array(
					'contenu' => $contenu,
					'points' => $points,
					'id_question' => $id_question
				));
		return $contenu;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function recup_examen_matiere($id_matiere){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT code FROM examen WHERE id_matiere=:id_matiere');
			$req->execute(array(
					'id_matiere' => $id_matiere
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function recup_nom_examen($code_examen){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT  FROM examen WHERE id_matiere=:id_matiere');
			$req->execute(array(
					'id_matiere' => $id_matiere
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function liste_etud_fini($code_examen, $id_classe){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT DISTINCT code_etudiant, prenom, nom FROM examen_fait INNER JOIN etudiant ON examen_fait.code_etudiant=etudiant.code WHERE code_examen=:code_examen AND id_classe=:id_classe');
			$req->execute(array(
					'code_examen' => $code_examen,
					'id_classe' => $id_classe
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function recup_donnees_epreuve($code_examen, $code_etudiant){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare("SELECT * FROM reponse_etudiant INNER JOIN question ON reponse_etudiant.id_question=question.id WHERE question.type='qr' AND code_examen=:code_examen AND code_etudiant=:code_etudiant");
			$req->execute(array(
					'code_examen' => $code_examen,
					'code_etudiant' => $code_etudiant
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function infos_prof($matr_prof){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare("SELECT * FROM prof WHERE matr=:matr_prof ");
			$req->execute(array(
					'matr_prof' => $matr_prof
				));
		$resultSet = $req->fetchAll();
		return $resultSet;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}
	
?>
