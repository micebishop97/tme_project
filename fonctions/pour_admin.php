<?php
	include('pour_tous.php');

	// Si l'administrateur existe
	function admin_existe($login, $mdp){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM admin WHERE login=:login AND mdp=:mdp');
			$req->execute(array(
					'login' => $login,
					'mdp' => $mdp
				));
		if ($req->fetch()) {
			return 1;
		}
		else{
			return 0;
		}
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}

	}

	// Ajouter Classe
	function ajouter_classe($nom){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO classe (nom) VALUES(:nom)');
			$req->execute(array(
					'nom' => $nom
				));
		return $nom;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}
	// Supprimer Classe
	function supprimer_classe($nom){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM classe WHERE nom=?');
			$req->execute(array($nom));
			return $nom;
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	
		}
	}


	// Liste des classes
	function liste_classes(){
		$bdd = connection_bdd();
		try {
			$req = $bdd->query('SELECT * FROM classe');
			$resultSet = $req->fetchAll();
			return $resultSet;
			$req->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));			
		}
	}

	// Ajouter un etudiant
	function ajouter_etudiant($nom, $prenom, $ddn, $adr, $numtel, $nom_classe){
		$bdd = connection_bdd();
		$code = 'e'.generateRandomString();
		$mdp = 'mdp'.generateRandomString();
		$id_classe = recup_id_classe($nom_classe);
		try {
			$req = $bdd->prepare('INSERT INTO etudiant (nom, prenom, ddn, adr, numtel, id_classe, code, mdp) VALUES(:nom, :prenom, :ddn, :adr, :numtel, :id_classe, :code, :mdp)');
			$req->execute(array(
					'nom' => $nom,
					'prenom' => $prenom,
					'ddn' => $ddn,
					'adr' => $adr,
					'numtel' => $numtel,
					'id_classe' => $id_classe,
					'code' => $code,
					'mdp' => $mdp
				));
			return $id_classe;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}


	function supprimer_etudiant($code_etudiant){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM etudiant WHERE code=?');
			$req->execute(array($code_etudiant));
			return $code_etudiant;
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	
		}
	}

	function liste_etudiants(){
		$bdd = connection_bdd();
		try {
			$req = $bdd->query('SELECT * FROM etudiant');
			$resultSet = $req->fetchAll();
			return $resultSet;
			$req->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));			
		}
	}

	function liste_etudiants_classe($id_classe){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM etudiant WHERE id_classe=:id_classe');
			$req->execute(array(
					'id_classe' => $id_classe
				));
			$resultSet = $req->fetchAll();
			return $resultSet;
			$req->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));			
		}
	}

	
	function recup_id_etudiant($nom_etudiant){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM etudiant WHERE nom=:nom');
			$req->execute(array(
					'nom' => $nom_etudiant
				));

		$resultSet = $req->fetchAll();
		foreach ($resultSet as $key => $value) {
			$res = $value['id'];
		}
		return $res;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}
	
	function ajouter_prof($nom, $prenom, $adr, $numtel){
		$bdd = connection_bdd();
		$matr = 'p'.generateRandomString();
		$mdp = 'mdp'.generateRandomString();
		try {
			$req = $bdd->prepare('INSERT INTO prof (nom, prenom, adr, numtel,matr, mdp) VALUES(:nom, :prenom, :adr, :numtel, :matr, :mdp)');
			$req->execute(array(
					'nom' => $nom,
					'prenom' => $prenom,
					'adr' => $adr,
					'numtel' => $numtel,	
					'matr' => $matr,
					'mdp' => $mdp
				));
			return $matr;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}
	function supprimer_prof($matr_prof){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM prof WHERE matr=?');
			$req->execute(array($matr_prof));
			return $matr_prof;
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	
		}
	}
	function liste_profs(){
		$bdd = connection_bdd();
		try {
			$req = $bdd->query('SELECT * FROM prof');
			$resultSet = $req->fetchAll();
			return $resultSet;
			$req->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));			
		}
	}
	function recup_id_prof($nom_prof){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM prof WHERE nom=:nom');
			$req->execute(array(
					'nom' => $nom_prof
				));

		$resultSet = $req->fetchAll();
		foreach ($resultSet as $key => $value) {
			$res = $value['id'];
		}
		return $res;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}
	
	function ajouter_matiere($nom){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO matiere(nom) VALUES(:nom)');
			$req->execute(array(
					'nom' => $nom,
				
				));
		$req->closeCursor();
		return $nom;


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}
	function supprimer_matiere($nom_matiere){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM matiere WHERE nom=?');
			$req->execute(array($nom_matiere));
			return $nom_matiere;
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	
		}
	}

	function liste_matieres(){
		$bdd = connection_bdd();
			try {
			$req = $bdd->query('SELECT * FROM matiere');
			$resultSet = $req->fetchAll();
			return $resultSet;
			$req->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));			
		}
	}

	function matiere_existe($nom){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM matiere WHERE nom=:nom ');
			$req->execute(array(
					'nom' => $nom,
				));
		if ($req->fetch()) {
			return 1;
		}
		else{
			return 0;
		}
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}

	}
	function assignation_matiere($matr_prof,$id_matiere,$id_classe)
	{
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO enseigner(matr_prof ,id_matiere ,id_classe) VALUES(:matr_prof,:id_matiere,:id_classe)');
			$req->execute(array(
					'matr_prof' => $matr_prof,
					'id_matiere' => $id_matiere,
					'id_classe' => $id_classe
				
				));
		return $matr_prof;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}	
	}
	function supp_assignation_matiere($matr_prof,$id_matiere,$id_classe){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM enseigner WHERE matr_prof=? AND id_matiere=? AND id_classe=?');
			$req->execute(array($matr_prof,$id_matiere,$id_classe));
			$req->closeCursor();
			return $matr_prof;
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	
		}
	}
	function assignation_matiere_existe($matr_prof,$id_matiere,$id_classe){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM enseigner WHERE matr_prof=:matr_prof AND id_matiere=id_matiere AND id_classe=id_classe');
			$req->execute(array(
					'matr_prof' => $matr_prof,
					'id_matiere' => $id_matiere,
					'id_classe' =>$id_classe
				));
		if ($req->fetch()) {
			return 1;
		}
		else{
			return 0;
		}
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
		}
	}

	function liste_assignations(){
		$bdd = connection_bdd();
			try {
			$req = $bdd->query('SELECT DISTINCT prof.matr AS matr_prof, prof.prenom AS prenom_prof, prof.nom AS nom_prof, matiere.nom AS nom_matiere, matiere.id AS id_matiere, classe.id AS id_classe, classe.nom AS nom_classe FROM enseigner INNER JOIN prof ON prof.matr=enseigner.matr_prof INNER JOIN matiere ON enseigner.id_matiere=matiere.id INNER JOIN classe ON enseigner.id_classe=classe.id');
			$resultSet = $req->fetchAll();
			return $resultSet;
			$req->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));			
		}
	}

	function recup_id_classe($nom_classe){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM classe WHERE nom=:nom_classe');
			$req->execute(array(
					'nom_classe' => $nom_classe
				));

		$resultSet = $req->fetchAll();
		return $resultSet[0]['id'];
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function recup_id_matiere($nom_matiere){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM matiere WHERE nom=:nom_matiere');
			$req->execute(array(
					'nom_matiere' => $nom_matiere
				));

		$resultSet = $req->fetchAll();
		return $resultSet[0]['id'];
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function recup_donnees_enseigner($matr_prof){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM enseigner WHERE matr_prof=:matr_prof');
			$req->execute(array(
					'matr_prof' => $matr_prof
				));

		$resultSet = $req->fetchAll();
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}
?>